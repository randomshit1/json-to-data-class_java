class Ekspedisi {
    private status status;
    private servicePackage servicePackage;
    private city city;
    private double weight;
    private int quantity;
    private double priceItem;
    private String productName;

    {
        status = new status();
        servicePackage = new servicePackage();
        city = new city();
        weight = 0;
        quantity = 0;
        priceItem = 0;
        productName = "";
    }

    public status getStatus() {
        return this.status;
    }

    public void setStatus(status status) {
        this.status = status;
    }

    public servicePackage getServicePackage() {
        return this.servicePackage;
    }

    public void setServicePackage(servicePackage servicePackage) {
        this.servicePackage = servicePackage;
    }

    public city getCity() {
        return this.city;
    }

    public void setCity(city city) {
        this.city = city;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPriceItem() {
        return this.priceItem;
    }

    public void setPriceItem(double priceItem) {
        this.priceItem = priceItem;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getTotal() {
        return (this.priceItem * this.quantity) + this.servicePackage.getValue();
    }
}

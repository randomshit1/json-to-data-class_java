
import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class App {

    public static void main(String[] args) throws Exception {
        final String FileInName = "src/EkspedisiImport.json";
        final String FileOutName = "src/EkspedisiExport.json";

        // -------------- PROSES IMPORT --------------
        Gson gson = new Gson();
        String file = FileInName;
        String json = new String(Files.readAllBytes(Paths.get(file)));
        // System.out.println(json);
        Ekspedisi[] data = gson.fromJson(json, Ekspedisi[].class);
        // -------------- PROSES IMPORT --------------

        // -------------- PROSES EXPORT --------------
        String jsonInString = gson.toJson(data);
        // System.out.println(jsonInString);
        try {
            new File(FileOutName);
            FileWriter myWriter = new FileWriter(FileOutName);
            myWriter.write(jsonInString);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        // -------------- PROSES EXPORT --------------

        // -------------- TAMPIL --------------
        System.out.println("Jumlah Pesanan : " + data.length);

        for (int i = 0; i < data.length; i++) {
            // productName | quantity | weight | destination |
            // servicePackage | service Value | Total
            System.out.println("===============");

            System.out.println("--- Pesanan ke " + (i + 1) + ": --- \n");

            System.out.println(
                    data[i].getProductName() + " | " + data[i].getQuantity() + " | " + data[i].getWeight() + " | "
                            + data[i].getCity().getDestination() + " | " + data[i].getServicePackage().getService()
                            + " | " + data[i].getServicePackage().getValue() + " | " + data[i].getTotal());

            System.out.println("===============\n");
        }
        // -------------- TAMPIL --------------
        System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        } catch (Exception e) {
        }
    }
}
